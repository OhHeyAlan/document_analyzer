from __future__ import absolute_import, unicode_literals

from django.utils.translation import ugettext_lazy as _

from permissions import PermissionNamespace

namespace = PermissionNamespace('document_anayzer', _('Document analyzer'))


permission_analyze_document = namespace.add_permission(
    name='analyze_document', label=_('Submit documents to analyze')
)
permission_analyzer_create = namespace.add_permission(
    name='analyzer_create', label=_('Create analyzer')
)
permission_analyzer_delete = namespace.add_permission(
    name='analyzer_delete', label=_('Delete analyzer')
)
permission_analyzer_edit = namespace.add_permission(
    name='analyzer_edit', label=_('Edit analyzer')
)
permission_analyzer_view = namespace.add_permission(
    name='analyzer_view', label=_('View analyzer')
)

permission_renaming_template_edit = namespace.add_permission(
    name='renaming_template_edit', label=_('Edit renaming templates')
)
