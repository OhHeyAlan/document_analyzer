from __future__ import unicode_literals

import datetime
import logging

import regex as re

logger = logging.getLogger(__name__)


class DateFind(object):
    def execute(self, document_version, parameter):
        ps = document_version.pages
        result = []
        # method, regextool = parameter.split(';', 1)
        # reobj = re.compile(regextool)

        for p in ps.all():
            logger.debug("running datefind on page {}.".format(p))
            # logger.debug(p.ocr_content.content)
            # matches = datefinder.find_dates(p.ocr_content.content, source=True, index=True, strict=True)
            df = DateFinder()
            for f in df.find(p.ocr_content.content):
                print f, type(f)

            # for match in dr.finditer(p.ocr_content.content):
            #    match_str = match.group(0)
            #    indices = match.span(0)
            #    logger.debug('match_str:{}, index:{}'.format(match_str, indices))
            #    # Get individual group matches
            #    captures = match.capturesdict()
            #    day = captures.get('day')
            #    month = captures.get('month')
            #    year = captures.get('year')

            #    logger.debug('{}-{}-{}'.format(year, month, day))

            # for idx, l in enumerate(matches):
            #    m, date_string, indices = l
            #    attr = 'Date' + str(idx)
            #    result.append((attr, m.strftime("%Y.%m.%d")))
            #    logger.debug('datefind date_string:{}indices:{}'.format(date_string, indices))
            #    logger.debug('datefind match:{}:{}'.format(attr, m))

        return result


class DateFinder():

    start = u'(?:\\s'
    end = u'\\b)'

    rg = {}

    rg['d'] = u'(?P<day>3[01]|[12][0-9]|0?[1-9])'  # without leading zero
    rg['dz'] = u'(?P<day>3[01]|[12][0-9]|0[1-9])'  # with leading zero

    rg['m'] = u'(?P<month>1[0-2]|0?[1-9])'  # without leading zero
    rg['mz'] = u'(?P<month>1[0-2]|0[1-9])'  # with leading zero

    rg['y'] = u'(?P<year>(?:[0-9]{2})?[0-9]{2})'  # yy or yyyy
    rg['x'] = u'(?:[/\-\.])'  # delimiter

    formats = ['d-x-m-x-y', 'y-x-m-x-d']

    @classmethod
    def generateRegex(cls, fmt):
        ret = cls.start
        for c in fmt.split('-'):
            ret += cls.rg[c]
        ret += cls.end
        logger.debug('regex:{}'.format(ret))
        return re.compile(ret, re.IGNORECASE | re.MULTILINE | re.UNICODE | re.DOTALL | re.VERBOSE)

    @classmethod
    def getxxDDMMYYY(cls):
        r = '(?:' + cls.start + cls.d + cls.dl + cls.m + cls.dl + cls.y + ')'
        logger.debug('regextool:{}'.format(r))
        return re.compile(r, re.IGNORECASE | re.MULTILINE | re.UNICODE | re.DOTALL | re.VERBOSE)

    def find(self, text):
        for f in DateFinder.formats:
            dr = DateFinder.generateRegex(f)
            for match in dr.finditer(text):
                match_str = match.group(0)
                indices = match.span(0)
                logger.debug('match_str:{}, index:{}'.format(match_str, indices))
                # Get individual group matches
                captures = match.capturesdict()
                day = int(captures.get('day')[0])
                month = int(captures.get('month')[0])
                year = int(captures.get('year')[0])
                ret = '{}-{}-{}'.format(year, month, day)
                logger.debug(ret)
                da = datetime.date(year, month, day)
                yield da
