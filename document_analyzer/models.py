from __future__ import unicode_literals

import logging

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from documents.models import DocumentVersion, DocumentType

from .managers import AnalyzerResultManager


from .settings import setting_analyzer_choices, setting_analyzer

logger = logging.getLogger(__name__)


@python_2_unicode_compatible
class Analyzer(models.Model):
    """
    Model to store the DocumentAnalyzer
    """

    document_types = models.ManyToManyField(
        DocumentType, related_name='analyzers',
        verbose_name=_('Document types')
    )

    label = models.CharField(
        max_length=32, unique=True, verbose_name=_('Label')
    )
    slug = models.SlugField(
        help_text=_('Do not use hypens, if you need spacing use underscores.'),
        verbose_name=_('Slug')
    )

    type = models.CharField(
        max_length=100, verbose_name=_('Type'))

    type = models.CharField(
        blank=True, choices=setting_analyzer_choices.value,
        default=setting_analyzer.value, max_length=50,
        verbose_name=_('Analyzer')
    )

    parameter = models.CharField(
        max_length=100, verbose_name=_('Parameter'))

    def get_document_types_not_in_analyzer(self):
        return DocumentType.objects.exclude(pk__in=self.document_types.all())

    def __str__(self):
        return self.label


@python_2_unicode_compatible
class Result(models.Model):
    """
    Model to store an result of DocumentAnalysis for a document version.
    """

    document_version = models.ForeignKey(
        DocumentVersion, related_name='analyzerresult',
        verbose_name=_('Document version')
    )

    analyzer = models.ForeignKey(
        Analyzer, related_name='analyzer',
        verbose_name=_('Analyzer')
    )

    parameter = models.CharField(max_length=255, verbose_name=_('Parameter'))
    value = models.CharField(max_length=255, verbose_name=_('Value'))

    objects = AnalyzerResultManager()

    class Meta:
        verbose_name = _('Document version Analyzer Result')
        verbose_name_plural = _('Document versions Analyzer Result')

    def __str__(self):
        return '{}: {}: {}'.format(self.analyzer, self.parameter, self.value)
